﻿using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using FileManager.Models;


namespace FileManager.DataAccess
{
    public interface IApplicationStorage
    {
        #region utils

        string GenerateSalt();
        string GetHash(string password, string salt);

        #endregion

        #region users

        User GetUser(int id);

        User GetUserByLogin(string login);

        User GetAuthentificatedUser(string login, string password);

        IEnumerable<User> GetAllUsers();

        void AddUser(User user);

        void DeleteUser(User user);

        #endregion

        #region files

        File GetFile(int id);

        System.IO.FileStream GetFileStream(File file);

        File GetFileByHash(string hash);

        void UploadFile(File fileDescription, System.IO.Stream stream);

        IEnumerable<File> GetAllFiles();

        IEnumerable<File> GetAllOwnedFiles(User user);
        
        IEnumerable<File> GetAllSharedFiles(User user);

        void AddFileToShared(File file, User user);

        void DeleteFile(File file);

        #endregion

        #region links

        Link GetLink(int id);

        Link GetLink(User user, File file);

        void DeleteLink(Link link);

        #endregion
    }

    public class ApplicationStorage : IApplicationStorage
    {
        private readonly SHA1 hashing;

        private readonly RandomNumberGenerator random;

        private readonly ApplicationContext context;

        private readonly string path;

        public ApplicationStorage(ApplicationContext context, SHA1 hashing, RandomNumberGenerator random, IFolderPath folder)
        {
            this.context = context;
            this.hashing = hashing;
            this.random = random;
            this.path = folder.Path;
        }

        #region utils

        public string GenerateSalt()
        {
            byte[] salt = new byte[256];
            random.GetBytes(salt);
            return new string(salt.Select(byt => (char)('a' + byt % 26)).ToArray());
        }

        public string GetHash(string password, string salt)
        {
            byte[] arrayToHash = (password + salt).Select(chr => (byte)chr).ToArray();
            byte[] hash = hashing.ComputeHash(arrayToHash);
            return new string(hash.Select(byt => (char)byt).ToArray());
        }

        private const int HashLength = 64;

        private string AddHash(File file)
        {
            byte[] seq = new byte[HashLength];
            random.GetBytes(seq);

            char[] hash = seq.Select(x => (char)('a' + (x % 26))).ToArray();

            file.Hash = new string(hash);
            return file.Hash;
        }

        #endregion

        #region users

        /// <summary>
        /// Получить пользователя по его Id
        /// </summary>
        /// <param name="id">Уникальный идентификатор пользователя</param>
        /// <returns></returns>
        public User GetUser(int id)
        {
            return context.Users.FirstOrDefault(user => user.Id == id);
        }

        /// <summary>
        /// Получить текущего юзера по его логину
        /// </summary>
        /// <param name="login">Уникальный логин</param>
        /// <returns></returns>
        public User GetUserByLogin(string login)
        {
            return context.Users.FirstOrDefault(user => user.Login == login);
        }

        /// <summary>
        /// Получить юзера по паре логин-пароль
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="passwordHash">Хеш пароля и соли</param>
        /// <returns></returns>
        public User GetAuthentificatedUser(string login, string password)
        {
            return context.Users.FirstOrDefault(u => u.Login == login && GetHash(password, u.Salt) == u.Password);
        }

        /// <summary>
        /// Получить всех пользователей из базы данных
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetAllUsers()
        {
            return context.Users.ToArray();
        }

        /// <summary>
        /// Добавить нового пользователя в базу
        /// </summary>
        /// <param name="user">Добавляемый пользователь</param>
        public void AddUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }
        
        /// <summary>
        /// Удалить юзера
        /// </summary>
        /// <param name="user">Удаляемый пользователь</param>
        public void DeleteUser(User user)
        {
            var relatedLinks = context.Links.Where(link => link.UserId == user.Id);
            context.Links.RemoveRange(relatedLinks);

            var ownedFiles = context.Files.Where(file => file.OwnerId == user.Id);
            context.Files.RemoveRange(ownedFiles);

            context.Users.Remove(user);
            context.SaveChanges();
        }

        #endregion

        #region files

        /// <summary>
        /// Получить файл по его Id
        /// </summary>
        /// <param name="id">Уникальный идентификатор файла</param>
        /// <returns></returns>
        public File GetFile(int id)
        {
            return context.Files.Include(x => x.Owner).FirstOrDefault(file => file.Id == id);
        }

        public System.IO.FileStream GetFileStream(File file)
        {
            return new System.IO.FileStream(
                System.IO.Path.Join(path, file.Hash), 
                System.IO.FileMode.Open, 
                System.IO.FileAccess.Read, 
                System.IO.FileShare.Read
            );
        }

        /// <summary>
        /// Получить файл по его хешу
        /// </summary>
        /// <param name="hash">Хеш, соответствующий файлу</param>
        /// <returns></returns>
        public File GetFileByHash(string hash)
        {
            return context.Files.FirstOrDefault(file => file.Hash == hash);
        }

        /// <summary>
        /// Загрузить файл в БД и в папку одновременно
        /// </summary>
        /// <param name="fileDescription">Модель добавляемого файла</param>
        /// <param name="realFile">Реальный файл, записываемый в директорию</param>
        public void UploadFile(File fileDescription, System.IO.Stream stream)
        {
            AddHash(fileDescription);

            context.Add(fileDescription);
            context.SaveChanges();

            // Добавление в папочку с файлами
            var physicalPath = System.IO.Path.Join(path, fileDescription.Hash);
            using (var fileStream = new System.IO.FileStream(physicalPath, System.IO.FileMode.Create))
            {
                stream.CopyTo(fileStream);
            }
        }

        public IEnumerable<File> GetAllFiles()
        {
            return context.Files.ToArray();
        }

        /// <summary>
        /// Получить все файлы для данного пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns></returns>
        public IEnumerable<File> GetAllOwnedFiles(User user)
        {
            return context.Files.Where(file => file.Owner == user).ToArray();
        }

        /// <summary>
        /// Получить все файлы, расшаренные данному пользователю
        /// </summary>
        /// <param name="user">Пользователь, которому доступны файлы по ссылке</param>
        /// <returns></returns>
        public IEnumerable<File> GetAllSharedFiles(User user)
        {
            return context.Links
                .Where(link => link.UserId == user.Id)
                .Join(context.Files,
                      link => link.FileId,
                      file => file.Id,
                      (link, file) => file
                )
                .ToArray();
        }

        /// <summary>
        /// Добавить файл в расшаренные данного юзера
        /// </summary>
        /// <param name="file">Добавляемый файл</param>
        /// <param name="user">Пользователь, который добавляет файл</param>
        public void AddFileToShared(File file, User user)
        {
            context.Links.Add(new Link() { File = file, User = user });
            context.SaveChanges();
        }

        /// <summary>
        /// Удалить файл
        /// </summary>
        /// <param name="id">Удаляемый файл</param>
        public void DeleteFile(File file)
        {            
            // Физическое стирание
            var physicalPath = System.IO.Path.Join(path, file.Hash);
            System.IO.File.Delete(physicalPath);

            var relatedLinks = context.Links.Where(link => link.FileId == file.Id);
            context.Links.RemoveRange(relatedLinks);

            context.Files.Remove(file);
            context.SaveChanges();
        }

        #endregion

        #region links

        /// <summary>
        /// Получить ссылку по ее Id
        /// </summary>
        /// <param name="id">Уникальный идентификатор ссылки</param>
        /// <returns></returns>
        public Link GetLink(int id)
        {
            return context.Links.FirstOrDefault(link => link.Id == id);
        }

        /// <summary>
        /// Получить ссылку
        /// </summary>
        /// <param name="user"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public Link GetLink(User user, File file)
        {
            return context.Links.Where(link => link.UserId == user.Id && link.FileId == file.Id).FirstOrDefault();
        }

        /// <summary>
        /// Удалить ссылку
        /// </summary>
        /// <param name="link">Удаляемая ссылка</param>
        public void DeleteLink(Link link)
        {
            context.Links.Remove(link);
        }

        #endregion
    }
}
