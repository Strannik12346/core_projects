﻿namespace FileManager.DataAccess
{
    public interface IFolderPath
    {
        string Path { get; set; }
    }

    public class FolderPath : IFolderPath
    {
        public string Path { get; set; }

        public FolderPath(string folderPath)
        {
            this.Path = folderPath;
        }
    }
}
