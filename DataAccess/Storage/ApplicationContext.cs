﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using FileManager.Models;


namespace FileManager.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        
        public DbSet<File> Files { get; set; }

        public DbSet<Link> Links { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Link>()
                .HasOne(l => l.User)
                .WithMany(u => u.SharedLinks)
                .OnDelete(DeleteBehavior.Restrict);
        }

        public ApplicationContext(DbContextOptions options) : base(options)
        {
            var mainUser = Users.Where(u => u.Login == "strannik12346").FirstOrDefault();
            mainUser.Permissions = Permission.Admin;
            this.SaveChanges();
        }
    }
}