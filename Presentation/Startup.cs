﻿using System.Globalization;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FileManager.DataAccess;
using FileManager.BusinessLogic;


namespace FileManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }

        public IHostingEnvironment Environment { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            // добавляем сервисы SignalR
            services.AddSignalR();

            // создаем DbContext
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(connection));
            
            // создаем путь к папке из конфигурации
            string path = Configuration.GetSection("ResourceFolders").GetValue<string>("DefaultFolder");
            string fullPath = System.IO.Path.Join(Environment.ContentRootPath, path);

            // создаем FolderPath
            services.AddSingleton<IFolderPath>(new FolderPath(fullPath));

            // создаем SHA1
            services.AddSingleton(SHA1.Create());

            // создаем RandomNumberGenerator 
            services.AddSingleton(RandomNumberGenerator.Create());

            // создаем Storage
            services.AddScoped<IApplicationStorage, ApplicationStorage>();

            // создаем сервисы
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IHomeService, HomeService>();
            services.AddScoped<IUserService, UserService>();

            // подключение ресурсов локализации
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddMvc().AddViewLocalization();

            // установка конфигурации подключения
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => 
                {
                    options.LoginPath = new PathString("/Account/Login");
                });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            var supportedCultures = new[]
            {
                new CultureInfo("ru"),
                new CultureInfo("en")
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("ru"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });

            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chat");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                       name: "userDetails",
                       template: "{controller}/{action}/{userName}",
                       defaults: new { controller = "User", action = "Details" },
                       constraints: new { controller = "User", action = "Details" }
                   );

                routes.MapRoute(
                    name: "addShared",
                    template: "{controller}/{action}/{hash}",
                    defaults: new { controller = "File", action = "Shared" },
                    constraints: new { controller = "File", action = "Shared"}
                );

                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new {controller = "Home", action = "Index"}
                );
            });
        }
    }
}