﻿using FileManager.Models;


namespace FileManager.Presentation.Models
{
    public class FileViewModel
    {
        public FileViewModel(File file)
        {
            this.Id = file.Id;
            this.Name = file.Name;
            this.Extension = file.Extension;
            this.SizeBytes = file.SizeBytes;
            this.OwnerId = file.OwnerId;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public int SizeBytes { get; set; }

        public int OwnerId { get; set; }
    }
}
