﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FileManager.Models;
using FileManager.BusinessLogic;
using FileManager.Presentation.Models;


namespace FileManager.Controllers.Api
{
    [ApiController]
    [Route("api/admin/files")]
    [Produces("application/json")]
    public class AdminFilesController : ControllerBase
    {
        private readonly IAdminService logic;

        public AdminFilesController(IAdminService logic)
        {
            this.logic = logic;
        }

        [Authorize]
        [HttpGet]
        public IEnumerable<FileViewModel> Get()
        {
            return logic.GetAllFilesForAdmin(User.Identity.Name).Select(f => new FileViewModel(f));
        }
        
        [Authorize]
        [HttpGet("{id}")]
        public FileViewModel Get(int id)
        {
            return new FileViewModel(logic.GetFileForAdmin(User.Identity.Name, id));
        }
        
        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return logic.DeleteFileForAdmin(User.Identity.Name, id)
                ? (ActionResult)Ok()
                : NotFound();
        }
    }
}