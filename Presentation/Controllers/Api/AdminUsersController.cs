﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FileManager.Models;
using FileManager.BusinessLogic;


namespace FileManager.Controllers.Api
{
    [ApiController]
    [Route("api/admin/users")]
    [Produces("application/json")]
    public class AdminUsersController : ControllerBase
    {
        private readonly IAdminService logic;

        public AdminUsersController(IAdminService logic)
        {
            this.logic = logic;
        }

        [Authorize]
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return logic.GetUsersForAdmin(User.Identity.Name);
        }
        
        [Authorize]
        [HttpGet("{id}")]
        public User Get(int id)
        {
            return logic.GetUserForAdmin(User.Identity.Name, id);
        }
        
        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return logic.DeleteUserForAdmin(User.Identity.Name, id)
                ? (ActionResult)Ok()
                : NotFound();
        }
    }
}