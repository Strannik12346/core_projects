﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using FileManager.BusinessLogic;


namespace FileManager.Controllers.Mvc
{
    public class AdminController : Controller
    {
        private readonly IAdminService service;

        public AdminController(IAdminService logic)
        {
            this.service = logic;
        }

        [Authorize]
        public IActionResult AllFiles()
        {
            ViewData["UserName"] = User.Identity.Name;
            return service.IsAdminOrModerator(User.Identity.Name)
                ? View() as IActionResult
                : NotFound() as IActionResult;
        }

        [Authorize]
        public IActionResult AllUsers()
        {
            ViewData["UserName"] = User.Identity.Name;
            return service.IsAdminOrModerator(User.Identity.Name)
                ? View() as IActionResult
                : NotFound() as IActionResult;
        }
    }
}