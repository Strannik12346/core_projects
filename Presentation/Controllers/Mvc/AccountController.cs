﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Localization;
using FileManager.Presentation.Models;
using FileManager.BusinessLogic;


namespace FileManager.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService service;

        private readonly IStringLocalizer<AccountController> localizer;

        public AccountController(IAccountService logic, IStringLocalizer<AccountController> localizer)
        {
            this.service = logic;
            this.localizer = localizer;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (service.TryAuthentificate(model.Login, model.Password))
                {
                    await Authenticate(model.Login); // аутентификация
                    return RedirectToAction("Index", "Home");
                }

                // Аутентификация не прошла
                if (!service.UserExists(model.Login))
                    ViewData["Error"] = localizer["NoUser"];
                else
                    ViewData["Error"] = localizer["IncorrectPassword"];
            }

            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (service.TryCreateNewUser(model.FirstName, model.LastName, model.Login, model.Password))
                {
                    await Authenticate(model.Login);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewData["Error"] = localizer["SuchUserExists"];
                }
            }
            else
            {
                ViewData["Error"] = localizer["PasswordsNotMatch"];
            }

            return View(model);
        }

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim> { new Claim(ClaimsIdentity.DefaultNameClaimType, userName) };

            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}