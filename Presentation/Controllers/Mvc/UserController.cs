﻿using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FileManager.BusinessLogic;


namespace FileManager.Controllers
{
    public class UserController : Controller
    {
        private readonly IStringLocalizer<FileController> localizer;

        private readonly IUserService service;

        public UserController(IUserService logic, IStringLocalizer<FileController> localizer)
        {
            this.service = logic;
            this.localizer = localizer;
        }

        [Authorize]
        public ActionResult Details(string userName)
        {
            var userToShow = service.GetUserByLogin(userName);

            ViewData["UserToShow"] = userToShow;
            ViewData["UserName"] = User.Identity.Name;

            return View();
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ViewData["UserName"] = User.Identity.Name;
            ViewData["Id"] = id;

            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            if (service.TryDeleteUser(User.Identity.Name, id))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["UserName"] = User.Identity.Name;
            ViewData["ErrorMessage"] = localizer["NoAuthorization"];
            return View("Error");
        }
    }
}