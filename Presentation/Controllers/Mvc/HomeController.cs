﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FileManager.BusinessLogic;
using FileManager.Models;


namespace FileManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeService service;

        public HomeController(IHomeService logic)
        {
            this.service = logic;
        }

        [Authorize]
        public IActionResult Index()
        {
            service.GetAllFilesForUser(User.Identity.Name, out IEnumerable<File> ownedFiles, out IEnumerable<File> sharedFiles);
            ViewData["OwnedFiles"] = ownedFiles;
            ViewData["SharedFiles"] = sharedFiles;
            ViewData["UserName"] = User.Identity.Name;

            return View();
        }

        [Authorize]
        public IActionResult About()
        {
            ViewData["UserName"] = User.Identity.Name;
            return View();
        }

        [Authorize]
        public IActionResult Contact()
        {
            ViewData["UserName"] = User.Identity.Name;
            return View();
        }
    }
}