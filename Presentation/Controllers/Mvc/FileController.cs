﻿using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FileManager.BusinessLogic;
using FileManager.Models;


namespace FileManager.Controllers
{
    public class FileController : Controller
    {
        private readonly IFileService service;

        private readonly IStringLocalizer<FileController> localizer;

        public FileController(IFileService logic, IStringLocalizer<FileController> localizer)
        {
            this.service = logic;
            this.localizer = localizer;
        }

        [Authorize]
        public ActionResult Details(int id)
        {
            if (service.TryGetFileDetails(User.Identity.Name, id, out File details))
            {
                ViewData["FileToShow"] = details;
                ViewData["UserName"] = User.Identity.Name;

                return View();
            }

            ViewData["UserName"] = User.Identity.Name;
            ViewData["ErrorMessage"] = localizer["NoAuthorization"];
            return View("Error");
        }

        // GET: File/Upload
        [Authorize]
        public ActionResult Upload()
        {
            ViewData["UserName"] = User.Identity.Name;
            return View();
        }

        // POST: File/Upload
        [HttpPost]
        [Authorize]
        public ActionResult Upload(IFormFile file)
        {
            if (service.TryUploadFile(file.FileName, (int)file.Length, User.Identity.Name, file.OpenReadStream()))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["UserName"] = User.Identity.Name;
            ViewData["ErrorMessage"] = localizer["UploadError"];
            return View("Error");
        }

        // GET: File/Download/5
        [Authorize]
        public ActionResult Download(int id)
        {
            if (service.TryDownloadFile(User.Identity.Name, id, out File fileDetails, out System.IO.FileStream stream))
            {
                return File(stream,
                        contentType: $"application/{fileDetails.Extension}",
                        fileDownloadName: $"{fileDetails.Name}.{fileDetails.Extension}");
            }
            
            ViewData["UserName"] = User.Identity.Name;
            ViewData["ErrorMessage"] = localizer["NoAuthorization"];
            return View("Error");
        }

        // GET: File/Delete/5
        [HttpGet]
        [Authorize]
        public ActionResult Delete(int id)
        {
            ViewData["UserName"] = User.Identity.Name;
            ViewData["Id"] = id;

            return View();
        }

        // POST: File/Delete/5
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            if (service.TryDeleteFile(User.Identity.Name, id))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["UserName"] = User.Identity.Name;
            ViewData["ErrorMessage"] = localizer["NoAuthorization"];
            return View("Error");
        }

        [Authorize]
        public ActionResult Share(int id)
        {
            if (service.TryShareFile(User.Identity.Name, id, out string fileHash))
            {
                string href = $"{Request.Host}/File/{nameof(Shared)}/{fileHash}";
                return Content($"{localizer["Href"]}: {href}");
            }

            ViewData["UserName"] = User.Identity.Name;
            ViewData["ErrorMessage"] = localizer["NoAuthorization"];
            return View("Error");
        }
        
        public ActionResult Shared(string hash)
        {
            if (service.AddSharedAuthorized(User.Identity.Name, hash))
            {
                return RedirectToAction("Index", "Home");
            }

            if (service.DownloadSharedAnonymous(hash, out File file, out System.IO.FileStream stream))
            {
                return File(
                    stream,
                    contentType: $"application/{file.Extension}",
                    fileDownloadName: $"{file.Name}.{file.Extension}"
                );
            }

            ViewData["UserName"] = User.Identity.Name;
            ViewData["ErrorMessage"] = localizer["AlreadyOwner"];
            return View("Error");
        }
    }
}