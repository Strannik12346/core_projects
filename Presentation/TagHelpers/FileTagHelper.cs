﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Localization;

namespace FileManager.TagHelpers
{
    public class FileTagHelper : TagHelper
    {
        private readonly IStringLocalizer<FileTagHelper> localizer;

        public FileTagHelper(IStringLocalizer<FileTagHelper> localizer)
        {
            this.localizer = localizer;
        }

        public string Name { get; set; }

        public string Href { get; set; }

        public string Sharing { get; set; }

        public string Downloading { get; set; }

        public string Deleting { get; set; }

        // <file name="name" href="href" sharing="sharing" deleting="deleting">
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "tr";
            output.TagMode = TagMode.StartTagAndEndTag;
            var format = "<td><a href={0}> {1} </a></td>";

            // Adding main link
            output.Content.AppendFormat(format, Href, Name);

            // Adding download link
            if (Downloading != null)
                output.Content.AppendFormat(format, Downloading, localizer["Download"]);

            // Adding share link
            if (Sharing != null)
                output.Content.AppendFormat(format, Sharing, localizer["Share"]);

            // Adding delete link
            if (Deleting != null)
                output.Content.AppendFormat(format, Deleting, localizer["Delete"]);
        }
    }
}
