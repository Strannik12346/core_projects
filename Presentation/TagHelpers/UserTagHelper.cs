﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Localization;

namespace FileManager.TagHelpers
{
    public class UserTagHelper : TagHelper
    {
        private readonly IStringLocalizer<UserTagHelper> localizer;

        public UserTagHelper(IStringLocalizer<UserTagHelper> localizer)
        {
            this.localizer = localizer;
        }

        public string Name { get; set; }

        public string Href { get; set; }

        public string Deleting { get; set; }

        // <user name="name" href="href" deleting="deleting">
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "tr";
            output.TagMode = TagMode.StartTagAndEndTag;

            // Adding main link
            output.Content.AppendFormat("<td><a href={0}>{1}</a></td>", Href, Name);

            // Adding link to deleting
            if (Deleting != null)
                output.Content.AppendFormat("<td><a href={0}> {1} </a></td>", Deleting, localizer["Delete"]);
        }
    }
}
