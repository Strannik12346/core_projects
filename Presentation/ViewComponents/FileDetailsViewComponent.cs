﻿using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Html;
using FileManager.Models;


namespace FileManager.ViewComponents
{
    public class FileDetailsViewComponent : ViewComponent
    {
        private readonly IStringLocalizer<FileDetailsViewComponent> localizer;

        public FileDetailsViewComponent(IStringLocalizer<FileDetailsViewComponent> localizer)
        {
            this.localizer = localizer;
        }

        public HtmlContentBuilder Invoke(File file)
        {
            HtmlContentBuilder htmlResult = new HtmlContentBuilder();

            var output = new (string key, string value)[]
            {
                ($"{localizer["Name"]}",              $"{file.Name}.{file.Extension}"),
                ($"{localizer["Size"]}",              $"{file.SizeBytes / 1024}Kb"),
                ($"{localizer["Owner"]}",             $"{file.Owner.Login}"),
                ($"{localizer["Permissions"]}",       $"{file.Owner.Permissions}")
            };

            htmlResult.AppendHtml(@"<table align='left' style='width: 40%; text-align: left; margin-bottom: 30px;'>");

            foreach (var line in output)
            {
                htmlResult.AppendFormat("<tr><td style='text-align: left;'> {0} </td> <td style='text-align: left;'> {1} </td> </tr>", line.key, line.value);
            }

            htmlResult.AppendHtml("</table>");
            return htmlResult;
        }
    }
}
