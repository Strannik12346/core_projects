﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;


namespace FileManager
{
    public class ChatHub : Hub
    {
        static readonly ICollection<string> Notifications;
        
        static ChatHub()
        {
            Notifications = new List<string>()
            {
                $"[{System.DateTime.Now.ToString()}]: test connection :))"
            };
        }

        public async Task SendMessage(string message)
        {
            message = $"[{System.DateTime.Now.ToString()}]: {message}";
            Notifications.Add(message);
            await Clients.All.SendAsync("ReceiveMessage", message);
        }

        public override async Task OnConnectedAsync()
        {
            foreach (var notification in Notifications)
            {
                await Clients.Caller.SendAsync("ReceiveMessage", notification);
            }

            await base.OnConnectedAsync();
        }
    }
}