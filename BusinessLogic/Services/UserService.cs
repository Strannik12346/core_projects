﻿using FileManager.DataAccess;
using FileManager.Models;


namespace FileManager.BusinessLogic
{
    public interface IUserService
    {
        User GetUserByLogin(string userName);

        bool TryDeleteUser(string executorName, int idToDelete);
    }

    public class UserService : IUserService
    {
        private readonly IApplicationStorage storage;

        public UserService(IApplicationStorage storage)
        {
            this.storage = storage;
        }

        public User GetUserByLogin(string userName)
        {
            return storage.GetUserByLogin(userName);
        }

        public bool TryDeleteUser(string executorName, int idToDelete)
        {
            var authorizedUser = storage.GetUserByLogin(executorName);
            var userToDelete = storage.GetUser(idToDelete);

            if (authorizedUser.Permissions == Permission.Admin)
            {
                storage.DeleteUser(userToDelete);
                return true;
            }

            return false;
        }
    }
}
