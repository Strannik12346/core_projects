﻿using FileManager.DataAccess;
using FileManager.Models;


namespace FileManager.BusinessLogic
{
    public interface IAccountService
    {
        bool TryCreateNewUser(string name, string surname, string login, string password);

        bool TryAuthentificate(string login, string password);

        bool UserExists(string login);
    }

    public class AccountService : IAccountService
    {
        private readonly IApplicationStorage storage;

        public AccountService(IApplicationStorage storage)
        {
            this.storage = storage;
        }

        public bool TryCreateNewUser(string name, string surname, string login, string password)
        {
            var user = storage.GetUserByLogin(login);
            if (user == null)
            {
                // Генерируем соль и хешируем
                string salt = storage.GenerateSalt();
                string hash = storage.GetHash(password, salt);

                // Добавляем пользователя в бд
                user = new User()
                {
                    FirstName = name,
                    LastName = surname,
                    Login = login,
                    Password = hash,
                    Salt = salt,
                    Permissions = Permission.User
                };

                storage.AddUser(user);
                return true;
            }

            return false;
        }

        public bool TryAuthentificate(string login, string password)
        {
            return storage.GetAuthentificatedUser(login, password) != null;
        }

        public bool UserExists(string login)
        {
            return storage.GetUserByLogin(login) != null;
        }
    }
}
