﻿using FileManager.DataAccess;
using FileManager.Models;


namespace FileManager.BusinessLogic
{
    public interface IFileService
    {
        bool TryGetFileDetails(string userName, int id, out File fileDetails);

        bool TryUploadFile(string fileName, int fileSize, string userName, System.IO.Stream fileStream);

        bool TryDownloadFile(string userName, int id, out File details, out System.IO.FileStream stream);

        bool TryDeleteFile(string userName, int id);

        bool TryShareFile(string userName, int id, out string fileHash);

        bool AddSharedAuthorized(string userName, string fileHash);

        bool DownloadSharedAnonymous(string fileHash, out File details, out System.IO.FileStream stream);
    }

    public class FileService : IFileService
    {
        private readonly IApplicationStorage storage;

        public FileService(IApplicationStorage storage)
        {
            this.storage = storage;
        }

        public bool TryGetFileDetails(string userName, int id, out File fileDetails)
        {
            var authorizedUser = storage.GetUserByLogin(userName);
            var file = storage.GetFile(id);
            var link = storage.GetLink(authorizedUser, file);

            var isAuthentificated = (
                authorizedUser.Permissions == Permission.Admin ||         // Авторизован админ 
                authorizedUser.Permissions == Permission.Moderator ||     // Авторизован модератор
                file.OwnerId == authorizedUser.Id ||                      // Авторизован владелец
                link != null                                              // Авторизован тот, кому расшарили
            );

            fileDetails = isAuthentificated ? file : null;

            return isAuthentificated;
        }

        public bool TryUploadFile(string fileName, int fileSize, string userName, System.IO.Stream fileStream)
        {
            try
            {
                string exactName, extension;

                // Добавление в БД записи
                if (fileName.Contains('.'))
                {
                    var pointIndex = fileName.LastIndexOf('.');
                    exactName = fileName.Substring(0, pointIndex);
                    extension = fileName.Substring(pointIndex + 1);
                }
                else
                {
                    exactName = fileName;
                    extension = "";
                }

                var fileToAdd = new File
                {
                    Name = exactName,
                    Extension = extension,
                    SizeBytes = fileSize,
                    Owner = storage.GetUserByLogin(userName),
                    IsPublic = false
                };

                storage.UploadFile(fileToAdd, fileStream);
                return true;
            }
            catch (System.Exception e)
            {
                return false;
            }
        }

        public bool TryDownloadFile(string userName, int id, out File details, out System.IO.FileStream stream)
        {
            var fileDescription = storage.GetFile(id);
            var fileStream = storage.GetFileStream(fileDescription);

            var authorizedUser = storage.GetUserByLogin(userName);
            var link = storage.GetLink(authorizedUser, fileDescription);

            if (authorizedUser.Id == fileDescription.OwnerId || link != null)
            {
                details = fileDescription;
                stream = fileStream;
                return true;
            }

            details = null;
            stream = null;
            return false;
        }

        public bool TryDeleteFile(string userName, int id)
        {
            var authorizedUser = storage.GetUserByLogin(userName);
            var file = storage.GetFile(id);

            if (authorizedUser.Id == file.OwnerId || authorizedUser.Permissions == Permission.Admin)
            {
                storage.DeleteFile(file);
                return true;
            }

            return false;
        }

        public bool TryShareFile(string userName, int id, out string fileHash)
        {
            var authorizedUser = storage.GetUserByLogin(userName);
            var file = storage.GetFile(id);

            if (authorizedUser.Id == file.OwnerId)
            {
                fileHash = file.Hash;
                return true;
            }

            fileHash = null;
            return false;
        }

        public bool AddSharedAuthorized(string userName, string fileHash)
        {
            var authorizedUser = storage.GetUserByLogin(userName);
            var file = storage.GetFileByHash(fileHash);

            if (storage.GetLink(authorizedUser, file) == null &&
                file.OwnerId != authorizedUser.Id)
            {
                storage.AddFileToShared(file, authorizedUser);
                return true;
            }

            return false;
        }

        public bool DownloadSharedAnonymous(string fileHash, out File details, out System.IO.FileStream stream)
        {
            var file = storage.GetFileByHash(fileHash);
            if (file == null)
            {
                details = null;
                stream = null;
                return false;
            }

            // Дадим неавторизованному пользователю
            // скачать расшаренный файл
            details = file;
            stream = storage.GetFileStream(file);
            return true;
        }
    }
}
