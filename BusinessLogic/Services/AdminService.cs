﻿using System.Collections.Generic;
using FileManager.DataAccess;
using FileManager.Models;


namespace FileManager.BusinessLogic
{
    public interface IAdminService
    {
        #region AdminFilesAPI

        IEnumerable<File> GetAllFilesForAdmin(string userName);

        File GetFileForAdmin(string userName, int id);

        bool DeleteFileForAdmin(string userName, int id);

        #endregion

        #region AdminUsersAPI

        IEnumerable<User> GetUsersForAdmin(string userName);

        User GetUserForAdmin(string userName, int id);

        bool DeleteUserForAdmin(string userName, int id);

        #endregion

        #region AdminPages

        bool IsAdminOrModerator(string userName);

        #endregion
    }

    public class AdminService : IAdminService
    {
        private readonly IApplicationStorage storage;

        public AdminService(IApplicationStorage storage)
        {
            this.storage = storage;
        }

        #region AdminFilesAPI

        private bool HasAdminPermissions(string userName)
        {
            var user = storage.GetUserByLogin(userName);
            return user.Permissions == Permission.Admin;
        }

        private bool HasModeratorPermissions(string userName)
        {
            var user = storage.GetUserByLogin(userName);
            return user.Permissions == Permission.Moderator
                || user.Permissions == Permission.Admin;
        }

        public IEnumerable<File> GetAllFilesForAdmin(string userName)
        {
            return HasModeratorPermissions(userName)
                ? storage.GetAllFiles()
                : null;
        }

        public File GetFileForAdmin(string userName, int id)
        {
            return HasModeratorPermissions(userName)
                ? storage.GetFile(id)
                : null;
        }

        public bool DeleteFileForAdmin(string userName, int id)
        {
            if (HasAdminPermissions(userName))
            {
                var file = storage.GetFile(id);
                storage.DeleteFile(file);
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region AdminUsersAPI

        public IEnumerable<User> GetUsersForAdmin(string userName)
        {
            return HasModeratorPermissions(userName)
                ? storage.GetAllUsers()
                : null;
        }

        public User GetUserForAdmin(string userName, int id)
        {
            return HasModeratorPermissions(userName)
                ? storage.GetUser(id)
                : null;
        }

        public bool DeleteUserForAdmin(string userName, int id)
        {
            if (HasAdminPermissions(userName))
            {
                var user = storage.GetUser(id);
                storage.DeleteUser(user);
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region AdminPages

        public bool IsAdminOrModerator(string userName)
        {
            var permissions = storage.GetUserByLogin(userName)?.Permissions;
            return permissions == Permission.Admin || permissions == Permission.Moderator;
        }

        #endregion
    }
}