﻿using System.Collections.Generic;
using FileManager.DataAccess;
using FileManager.Models;


namespace FileManager.BusinessLogic
{
    public interface IHomeService
    {
        void GetAllFilesForUser(string userName, out IEnumerable<File> ownedFiles, out IEnumerable<File> sharedFiles);
    }

    public class HomeService : IHomeService
    {
        private readonly IApplicationStorage storage;

        public HomeService(IApplicationStorage storage)
        {
            this.storage = storage;
        }

        public void GetAllFilesForUser(string userName, out IEnumerable<File> ownedFiles, out IEnumerable<File> sharedFiles)
        {
            var user = storage.GetUserByLogin(userName);
            ownedFiles = storage.GetAllOwnedFiles(user);
            sharedFiles = storage.GetAllSharedFiles(user);
        }
    }
}
