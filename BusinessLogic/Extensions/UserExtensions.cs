﻿using FileManager.Models;


namespace FileManager.BusinessLogic.Extensions
{
    public static class UserExtensions
    {
        public static string GetHref(this User user)
        {
            return $"User/Details/{user.Id}";
        }

        public static string GetDeletingHref(this User user)
        {
            return $"User/Delete/{user.Id}";
        }
    }
}