﻿using FileManager.Models;


namespace FileManager.BusinessLogic.Extensions
{
    public static class FileExtensions
    {
        public static string GetHref(this File file)
        {
            return $"File/Details/{file.Id}";
        }

        public static string GetSharingHref(this File file)
        {
            return $"File/Share/{file.Id}";
        }

        public static string GetDeletingHref(this File file)
        {
            return $"File/Delete/{file.Id}";
        }

        public static string GetDownloadingHref(this File file)
        {
            return $"File/Download/{file.Id}";
        }
    }
}