﻿namespace FileManager.Models
{
    public class Link
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        
        public User User { get; set; }

        public int FileId { get; set; }
        
        public File File { get; set; }
    }
}